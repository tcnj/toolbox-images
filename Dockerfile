ARG BASE=debian:bullseye

FROM $BASE

LABEL com.github.containers.toolbox="true" \
      com.github.debarshiray.toolbox="true"

RUN apt update && apt -y upgrade && apt -y install libcap2-bin sudo libnss-myhostname && apt clean
RUN echo '%sudo ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/toolbox
